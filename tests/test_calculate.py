import sys
import pytest

sys.path.append("pycalc")

from modules.calculate import Calculate


def test_calculate_add():
    """Functional test for add feature"""
    calculate_instance = Calculate(["1", "+", "2"])
    assert calculate_instance.calculate() == "3.0"


def test_calculate_subtract():
    """Functional test for subtract feature"""
    calculate_instance = Calculate(["1", "-", "2"])
    assert calculate_instance.calculate() == "-1.0"


def test_calculate_multiply():
    """Functional test for multiply feature"""
    calculate_instance = Calculate(["1", "*", "2"])
    assert calculate_instance.calculate() == "2.0"


def test_calculate_divide():
    """Functional test for divide feature"""
    calculate_instance = Calculate(["1", "/", "2"])
    assert calculate_instance.calculate() == "0.5"


def test_calculate_power():
    """Functional test for power feature"""
    calculate_instance = Calculate(["1", "^", "2"])
    assert calculate_instance.calculate() == "1.0"


def test_calculate_priority():
    """Functional test for priority feature"""
    calculate_instance = Calculate(["1", "+", "2", "*", "3"])
    assert calculate_instance.calculate() == "7.0"


def test_calculate_brackets():
    """Functional test for brackets feature"""
    calculate_instance = Calculate(["1", "+", "(", "2", "*", "3", ")"])
    assert calculate_instance.calculate() == "7.0"
