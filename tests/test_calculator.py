import sys
import pytest

sys.path.append("pycalc")

from modules import calculator


def test_calculator_add():
    """Test the add function"""
    assert calculator.add(1, 2) == 3


def test_calculator_add_negative():
    """Test the add function with negative numbers"""
    assert calculator.add(-1, -2) == -3


def test_calculator_subtract():
    """Test the subtract function"""
    assert calculator.subtract(1, 2) == 1


def test_calculator_subtract_negative():
    """Test the subtract function with negative numbers"""
    assert calculator.subtract(-1, -2) == 1


def test_calculator_multiply():
    """Test the multiply function"""
    assert calculator.multiply(1, 2) == 2


def test_calculator_multiply_negative():
    """Test the multiply function with negative numbers"""
    assert calculator.multiply(-1, -2) == 2


def test_calculator_divide():
    """Test the divide function"""
    assert calculator.divide(1, 2) == 0.5


def test_calculator_divide_negative():
    """Test the divide function with negative numbers"""
    assert calculator.divide(-1, -2) == 0.5


def test_calculator_divide_by_zero():
    """Test the divide function raise error when divide by zero"""
    with pytest.raises(ZeroDivisionError):
        calculator.divide(1, 0)


def test_calculator_divide_by_zero_message():
    """Test the divide function raise error when divide by zero"""
    with pytest.raises(ZeroDivisionError) as e:
        calculator.divide(1, 0)
    assert str(e.value) == "Division by zero is not allowed"


def test_calculator_percent():
    """Test the percent function"""
    assert calculator.percent(1, 2) == 0.02


def test_calculator_percent_negative():
    """Test the percent function with negative numbers"""
    assert calculator.percent(-1, -2) == 0.02


def test_calculator_sqr():
    """Test the sqr function"""
    assert calculator.sqr(4) == 16


def test_calculator_sqr_negative():
    """Test the sqr function with negative numbers"""
    assert calculator.sqr(-4) == 16


def test_calculator_sqr_root():
    """Test the sqr_root function"""
    assert calculator.sqr_root(4) == 2


def test_calculator_sqr_root_negative():
    """Test the sqr_root function with negative numbers"""
    with pytest.raises(ValueError) as e:
        calculator.sqr_root(-4)
    assert str(e.value) == "Cannot calculate square root from negative number"


def test_calculator_power():
    """Test the power function"""
    assert calculator.power(2, 2) == 4


def test_calculator_power_with_negative():
    """Test the power function with negative numbers"""
    assert calculator.power(-2, 2) == 4
