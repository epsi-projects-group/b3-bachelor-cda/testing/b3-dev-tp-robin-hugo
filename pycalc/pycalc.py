import typer
from modules import calculator
from modules.calculate import Calculate

app = typer.Typer(
    name="Pycalc CLI",
    help="Pycalc is a CLI calculator to do simple operations.",
    rich_markup_mode="rich",
    epilog="Made with :heart:  by Ahmosys (H.R)",
)


@app.command()
def add(
    a: float = typer.Argument(
        ...,
        help="The first number.",
    ),
    b: float = typer.Argument(
        ...,
        help="The second number.",
    ),
):
    """Add two numbers"""
    typer.echo(calculator.add(a, b))


@app.command()
def subtract(
    a: float = typer.Argument(
        ...,
        help="The first number.",
    ),
    b: float = typer.Argument(
        ...,
        help="The second number.",
    ),
):
    """Subtract two numbers"""
    typer.echo(calculator.subtract(a, b))


@app.command()
def multiply(
    a: float = typer.Argument(
        ...,
        help="The first number.",
    ),
    b: float = typer.Argument(
        ...,
        help="The second number.",
    ),
):
    """Multiply two numbers"""
    typer.echo(calculator.multiply(a, b))


@app.command()
def divide(
    a: float = typer.Argument(
        ...,
        help="The first number.",
    ),
    b: float = typer.Argument(
        ...,
        help="The second number.",
    ),
):
    """Divide two numbers"""
    typer.echo(calculator.divide(a, b))


@app.command()
def percent(
    a: float = typer.Argument(
        ...,
        help="The first number (percent out of 100).",
    ),
    b: float = typer.Argument(
        ...,
        help="The second number.",
    ),
):
    """Percent of two numbers"""
    typer.echo(calculator.percent(a, b))


@app.command()
def power(
    a: float = typer.Argument(
        ...,
        help="The number.",
    ),
    b: float = typer.Argument(
        ...,
        help="The exponent.",
    ),
):
    """Power a number"""
    typer.echo(calculator.power(a, b))


@app.command()
def sqr(
    a: float = typer.Argument(
        ...,
        help="The number.",
    ),
):
    """Square a number"""
    typer.echo(calculator.sqr(a))


@app.command()
def sqr_root(
    a: float = typer.Argument(
        ...,
        help="The number.",
    ),
):
    """Square root of a number"""
    typer.echo(calculator.sqr_root(a))


@app.command()
def evaluate(
    expression: str = typer.Argument(
        ...,
        help="The expression to evaluate.",
    ),
):
    """Evaluate an expression"""
    calculate_instance = Calculate([*expression])
    typer.echo(calculate_instance.calculate())


if __name__ == "__main__":
    app()
