import math
from modules import calculator


class Calculate:

    _get_value = {
        "sin": lambda x: math.sin(math.radians(x)),
        "cos": lambda x: math.cos(math.radians(x)),
        "tan": lambda x: math.tan(math.radians(x)),
        "asin": lambda x: math.degrees(math.asin(x)),
        "acos": lambda x: math.degrees(math.acos(x)),
        "atan": lambda x: math.degrees(math.atan(x)),
        "!": lambda x: math.factorial(x),
        "log": lambda x: math.log10(x),
        "ln": lambda x: math.log(x),
        "^": lambda x, y: calculator.power(x, y),
        "/": lambda x, y: calculator.divide(x, y),
        "*": lambda x, y: calculator.multiply(x, y),
        "+": lambda x, y: calculator.add(x, y),
        "-": lambda x, y: calculator.subtract(x, y),
    }
    __unary_operators = ["sin", "cos", "tan", "asin", "acos", "atan", "!", "log", "ln"]
    __operators: list = [
        "(",
        "sin",
        "cos",
        "tan",
        "asin",
        "acos",
        "atan",
        "!",
        "log",
        "ln",
        "^",
        "/",
        "*",
        "+",
        "-",
    ]

    def __init__(self, expr_as_list: list):
        self.expr_as_list = expr_as_list

    def calculate(self) -> str:
        left_paren = self.__operators[0]
        while left_paren in self.expr_as_list:
            expression = self.__bracket_balencer(self.expr_as_list.copy())
            left_p = expression.index("(")
            right_p = len(expression) - list(reversed(expression)).index(")") - 1
            sub_expression = expression[left_p + 1 : right_p]
            new_instance = Calculate(sub_expression)
            self.expr_as_list[left_p : right_p + 1] = [new_instance.calculate()]

        self._extracted_from_calculate_16(1, 10)
        exp_op = self.__operators[10]
        gen_exp = self.__create_op_gen(exp_op)
        if any(gen_exp):
            self.__call_partial_calculate(exp_op)

        self._extracted_from_calculate_16(11, 13)
        self._extracted_from_calculate_16(13, 15)
        if len(self.expr_as_list) == 1:
            return self.expr_as_list[0]
        current_expression = self.expr_as_list
        raise ValueError(current_expression)

    def _extracted_from_calculate_16(self, arg0, arg1):
        unary_op = self.__operators[arg0:arg1]
        gen_unary = self.__create_op_gen(unary_op)
        if any(gen_unary):
            self.__call_partial_calculate(unary_op)

    def __partial_calculate(self, index: int) -> None:
        operator = self.expr_as_list[index]
        if operator in self.__unary_operators:
            if operator != "!":
                unary_operand = float(self.expr_as_list[index + 1])
                sub_result = self._get_value[operator](unary_operand)
                self.expr_as_list[index : index + 2] = [str(sub_result)]
            else:
                unary_operand = int(self.expr_as_list[index - 1])
                sub_result = self._get_value[operator](unary_operand)
                self.expr_as_list[index - 1 : index + 1] = [str(sub_result)]
            return

        left_operand = float(self.expr_as_list[index - 1])
        try:
            right_operand = float(self.expr_as_list[index + 1])
            sub_result = self._get_value[operator](left_operand, right_operand)
            self.expr_as_list[index - 1 : index + 2] = [str(sub_result)]
        except IndexError:
            self.expr_as_list[index - 1 : index + 2] = [str(left_operand)]

    def __bracket_balencer(self, expression: list):
        left_paren_count = expression.count("(")
        right_paren_count = expression.count(")")
        if left_paren_count > right_paren_count:
            count_diff = left_paren_count - right_paren_count
            right_paren = ")"
            expression.extend(right_paren * count_diff)
            return expression
        return expression

    def __call_partial_calculate(self, found_operator: str):
        FOUND_OPERATOR_EXISTS = True
        while FOUND_OPERATOR_EXISTS:
            for index, operator in enumerate(self.expr_as_list):
                if operator in found_operator:
                    self.__partial_calculate(index)
                    break
            else:
                FOUND_OPERATOR_EXISTS = False

    def __create_op_gen(self, operator: list):
        return (g for g in operator if g in self.expr_as_list)
