def subtract(a: float, b: float) -> float:
    """Subtract two numbers

    Args:
        a (float): First number
        b (float): Second number

    Returns:
        float: Result of subtraction
    """
    return a - b


def multiply(a: float, b: float) -> float:
    """Multiply two numbers

    Args:
        a (float): First number
        b (float): Second number

    Returns:
        float: Result of multiplication
    """
    return a * b


def add(a: float, b: float) -> float:
    """Add two numbers

    Args:
        a (float): First number
        b (float): Second number

    Returns:
        float: Result of addition
    """
    return a + b


def divide(a: float, b: float) -> float:
    """Divide two numbers

    Args:
        a (float): First number
        b (float): Second number

    Returns:
        float: Result of division
    """
    try:
        return a / b
    except ZeroDivisionError as e:
        raise ZeroDivisionError("Division by zero is not allowed") from e


def percent(a: float, b: float) -> float:
    """Calculate the percentage of a number

    Args:
        a (float): First number (percent)
        b (float): Second number

    Returns:
        float: Result of percentage calculation
    """
    return a * b / 100


def evaluate(expression: str) -> float:
    """Evaluate the expression without taking into account the priority of operations 🥲

    Args:
        expression (str): Expression to evaluate

    Returns:
        float: Result of evaluation
    """


def sqr(a: float) -> float:
    """Calculate the square of a number

    Args:
        a (float): Number

    Returns:
        float: Result of square calculation
    """
    return a**2


def sqr_root(a: float) -> float:
    """Calculate the square root of a number

    Args:
        a (float): Number

    Returns:
        float: Result of square root calculation
    """
    if a < 0:
        raise ValueError("Cannot calculate square root from negative number")
    return a**0.5


def power(a: float, b: float) -> float:
    """Calculate the power of a number

    Args:
        a (float): Number
        b (float): Power

    Returns:
        float: Result of power calculation
    """
    return a**b
