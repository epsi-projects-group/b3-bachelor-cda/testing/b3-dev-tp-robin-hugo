# 🧮 Pycalc

A command line calculator to do mathematical operations !


```bash
 Usage: pycalc.py [OPTIONS] COMMAND [ARGS]...

 Pycalc is a CLI calculator to perform mathematical operations.

╭─ Options ──────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ --install-completion          Install completion for the current shell.                                                │
│ --show-completion             Show completion for the current shell, to copy it or customize the installation.         │
│ --help                        Show this message and exit.                                                              │
╰────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯
╭─ Commands ─────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ add                               Add two numbers                                                                      │
│ divide                            Divide two numbers                                                                   │
│ evaluate                          Evaluate an expression                                                               │
│ multiply                          Multiply two numbers                                                                 │
│ percent                           Percent of two numbers                                                               │
│ power                             Power a number                                                                       │
│ sqr                               Square a number                                                                      │
│ sqr-root                          Square root of a number                                                              │
│ subtract                          Subtract two numbers                                                                 │
╰────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯

 Made with ❤  by Ahmosys (H.R)
```

## Installation

Install Pycalc with pip :
```bash
  git clone https://gitlab.com/epsi-projects-group/b3-bachelor-cda/testing/b3-c3-dev-tu-robin-hugo.git
  cd b3-c3-dev-tu-robin-hugo/
  pip install -r requirements.txt
  cd pycalc/
  python pycalc.py
```
    
## Usage/Examples

Add two numbers :
```bash
python pytest.py add number_a number_b
```
Subtract two numbers :
```bash
python pytest.py subtract number_a number_b
```
For get help :
```bash
python pytest.py --help
```

## Running Tests

⚠️ Warning be sure you are in the root folder ```b3-dev-tp-robin-hugo/``` of the project then run the following command :

```bash
  pytest -v
```
_This will run the test suite in verbose mode via the pytest module_.

## Test plan

- #### BDD Method

| Functionality to be tested | Description                                                  | Reaction in case of success                                                            | Reaction in case of failure                                                                                     |
| -------------------------- | ------------------------------------------------------------ | -------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------- |
| Adding two numbers.        | Allows the user to perform additions between two numbers.    | If successful, this will return the result of the addition between the two numbers.    | In case of failure nothing will happen.                                                                         |
| Subtract two numbers.      | Allows the user to perform subtractions between two numbers. | If successful, this will return the result of the subtraction between the two numbers. | In case of failure nothing will happen.                                                                         |
| Multiply two numbers.      | Allows the user to perform subtractions between two numbers. | If successful, this will return the result of the subtraction between the two numbers. | In case of failure nothing will happen.                                                                         |
| Divide two numbers.        | Allows the user to perform divisions between two numbers.    | If successful, this will return the result of the division between the two numbers.    | In case of failure, the function captures the error and thus raises an error to warn the user of a malfunction. |
| Percent a number.          | Allows the user to calculate a percentage of a figure.       | If successful, it returns the result of the percentage value of the value entered.     | In case of failure nothing will happen.                                                                         |

- #### TDD Method

| # | User Story                                                                                  | Acceptance Criteria                                                                                                   |
| - | ------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------- |
| 1 | As an user, I need to **calculate the square** of number so that I can integrate in operations. | Ensure the user is able to :<br> - Calculate the square of number.<br>- Calculate the square within an expression.
| 2 | As an user, I need to **calculate the square root** of number so that I can integrate in operations. | Ensure the user is able to :<br> - Calculate the square root of number.<br>- Calculate the square root within an expression.
